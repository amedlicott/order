package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.DeleteOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.DeleteOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteOrderTests {

	@Test
	public void testOperationDeleteOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		DeleteOrderInputParametersDTO inputs = new DeleteOrderInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteOrderReturnDTO returnValue = serviceDefaultImpl.deleteorder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}