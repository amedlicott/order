package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.FindOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.FindOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindOrderTests {

	@Test
	public void testOperationFindOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		FindOrderInputParametersDTO inputs = new FindOrderInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindOrderReturnDTO returnValue = serviceDefaultImpl.findorder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}