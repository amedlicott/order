package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateSimpleOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateSimpleOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateSimpleOrderTests {

	@Test
	public void testOperationCreateSimpleOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		CreateSimpleOrderInputParametersDTO inputs = new CreateSimpleOrderInputParametersDTO();
		inputs.setSku(null);
		inputs.setCustomer(null);
		CreateSimpleOrderReturnDTO returnValue = serviceDefaultImpl.createSimpleOrder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}