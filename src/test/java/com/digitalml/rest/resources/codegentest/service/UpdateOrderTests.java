package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.UpdateOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.UpdateOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateOrderTests {

	@Test
	public void testOperationUpdateOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		UpdateOrderInputParametersDTO inputs = new UpdateOrderInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		UpdateOrderReturnDTO returnValue = serviceDefaultImpl.updateorder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}