package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Request:
{
  "required": [
    "order"
  ],
  "type": "object",
  "properties": {
    "order": {
      "$ref": "Order"
    }
  }
}
*/

public class Request {

	@Size(max=1)
	@NotNull
	private com.digitalml.www.information.model.retail.Order order;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    order = new com.digitalml.www.information.model.retail.Order();
	}
	public com.digitalml.www.information.model.retail.Order getOrder() {
		return order;
	}
	
	public void setOrder(com.digitalml.www.information.model.retail.Order order) {
		this.order = order;
	}
}