package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.OrderService;
	
import com.digitalml.rest.resources.codegentest.service.OrderService.GetASpecificOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.GetASpecificOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.GetASpecificOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.FindOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.FindOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.FindOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.UpdateOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.UpdateOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.UpdateOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.ReplaceOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.ReplaceOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.ReplaceOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.DeleteOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.DeleteOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.DeleteOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateSimpleOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateSimpleOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateSimpleOrderInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Order
	 * Access and update of order information.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	

	@Path("/")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class OrderResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private OrderService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private OrderService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof OrderService)) {
			LOGGER.error(implementationClass + " is not an instance of " + OrderService.class.getName());
			return null;
		}

		return (OrderService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificorder
		Gets order details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/order/{id}")
	public javax.ws.rs.core.Response getaspecificorder(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificOrderInputParametersDTO inputs = new OrderService.GetASpecificOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificOrderReturnDTO returnValue = delegateService.getaspecificorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findorder
		Gets a collection of order details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/order")
	public javax.ws.rs.core.Response findorder(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindOrderInputParametersDTO inputs = new OrderService.FindOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindOrderReturnDTO returnValue = delegateService.findorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateorder
		Updates order

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/order/{id}")
	public javax.ws.rs.core.Response updateorder(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateOrderInputParametersDTO inputs = new OrderService.UpdateOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateOrderReturnDTO returnValue = delegateService.updateorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceorder
		Replaces order

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/order/{id}")
	public javax.ws.rs.core.Response replaceorder(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceOrderInputParametersDTO inputs = new OrderService.ReplaceOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceOrderReturnDTO returnValue = delegateService.replaceorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createorder
		Creates order

	Non-functional requirements:
	*/
	
	@POST
	@Path("/order")
	public javax.ws.rs.core.Response createorder(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateOrderInputParametersDTO inputs = new OrderService.CreateOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateOrderReturnDTO returnValue = delegateService.createorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteorder
		Deletes order

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/order/{id}")
	public javax.ws.rs.core.Response deleteorder(
		@PathParam("id")@NotEmpty String id) {

		DeleteOrderInputParametersDTO inputs = new OrderService.DeleteOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteOrderReturnDTO returnValue = delegateService.deleteorder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createSimpleOrder
		Creates order

	Non-functional requirements:
	*/
	
	@POST
	@Path("/order")
	public javax.ws.rs.core.Response createSimpleOrder(
		@QueryParam("sku") String sku,
		@QueryParam("customer") String customer) {

		CreateSimpleOrderInputParametersDTO inputs = new OrderService.CreateSimpleOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSku(sku);
		inputs.setCustomer(customer);
	
		try {
			CreateSimpleOrderReturnDTO returnValue = delegateService.createSimpleOrder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}