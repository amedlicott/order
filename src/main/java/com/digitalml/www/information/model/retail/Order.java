package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Order:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "customerID": {
      "type": "string"
    },
    "orderDate": {
      "type": "string",
      "format": "date"
    },
    "shipping": {
      "$ref": "Address"
    },
    "billing": {
      "$ref": "Address"
    },
    "price": {
      "$ref": "Price"
    },
    "items": {
      "type": "array",
      "items": {
        "$ref": "Item"
      }
    },
    "status": {
      "type": "string"
    }
  }
}
*/

public class Order {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String customerID;

	@Size(max=1)
	private Date orderDate;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Address shipping;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Address billing;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Price price;

	@Size(max=1)
	private List<com.digitalml.www.information.model.retail.Item> items;

	@Size(max=1)
	private String status;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    customerID = null;
	    
	    address = new com.digitalml.www.information.model.retail.Address();
	    address = new com.digitalml.www.information.model.retail.Address();
	    price = new com.digitalml.www.information.model.retail.Price();
	    items = new ArrayList<com.digitalml.www.information.model.retail.Item>();
	    status = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomerID() {
		return customerID;
	}
	
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public com.digitalml.www.information.model.retail.Address getShipping() {
		return shipping;
	}
	
	public void setShipping(com.digitalml.www.information.model.retail.Address shipping) {
		this.shipping = shipping;
	}
	public com.digitalml.www.information.model.retail.Address getBilling() {
		return billing;
	}
	
	public void setBilling(com.digitalml.www.information.model.retail.Address billing) {
		this.billing = billing;
	}
	public com.digitalml.www.information.model.retail.Price getPrice() {
		return price;
	}
	
	public void setPrice(com.digitalml.www.information.model.retail.Price price) {
		this.price = price;
	}
	public List<com.digitalml.www.information.model.retail.Item> getItems() {
		return items;
	}
	
	public void setItems(List<com.digitalml.www.information.model.retail.Item> items) {
		this.items = items;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}