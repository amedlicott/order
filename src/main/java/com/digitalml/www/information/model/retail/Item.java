package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Item:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "cost": {
      "$ref": "Price"
    },
    "status": {
      "type": "string"
    },
    "supplier": {
      "type": "string"
    },
    "sku": {
      "type": "string"
    },
    "type": {
      "type": "string"
    },
    "price": {
      "$ref": "Price"
    },
    "weight": {
      "type": "string"
    }
  }
}
*/

public class Item {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Price cost;

	@Size(max=1)
	private String status;

	@Size(max=1)
	private String supplier;

	@Size(max=1)
	private String sku;

	@Size(max=1)
	private String type;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Price price;

	@Size(max=1)
	private String weight;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    description = null;
	    price = new com.digitalml.www.information.model.retail.Price();
	    status = null;
	    supplier = null;
	    sku = null;
	    type = null;
	    price = new com.digitalml.www.information.model.retail.Price();
	    weight = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public com.digitalml.www.information.model.retail.Price getCost() {
		return cost;
	}
	
	public void setCost(com.digitalml.www.information.model.retail.Price cost) {
		this.cost = cost;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSupplier() {
		return supplier;
	}
	
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSku() {
		return sku;
	}
	
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public com.digitalml.www.information.model.retail.Price getPrice() {
		return price;
	}
	
	public void setPrice(com.digitalml.www.information.model.retail.Price price) {
		this.price = price;
	}
	public String getWeight() {
		return weight;
	}
	
	public void setWeight(String weight) {
		this.weight = weight;
	}
}